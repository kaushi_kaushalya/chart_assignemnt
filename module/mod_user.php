<?php

/*
 * mod_user.php
 * 2019-10-073
 * kaushalya
 */

class mod_user extends mySQL {

    function getlogedUserDetails($userName) { #get user by User name
        $val = [ 'success' => false,'info' =>'error in db','data'=> []];
        try{
            $dbConn = new mySQL();
            $array = [];
            $selectFull = "select * from tbl_user where user_name='" . $userName . "' and status = 1"; #select query
           
            $dbConn->execute($selectFull, $result);
            while ($row = mysqli_fetch_array($result)) {
                $array['id'] = $row['id'];
                $array['userName'] = $row['user_name'];
                $array['fullName'] = $row['full_name'];
                $array['password'] = $row['password'];
            }
            $val = [ 'success' => true,'info' =>'success','data'=> $array];
        }catch(Exception $ex){
            $val = [ 'success' => true,'info' => $ex->getMessage(),'data'=> $array];
        }
        return $val;
    }

    function getUserList() { #get user List
        $val = [];
        $dbConn = new mySQL();
        $selectFull = "select * from tbl_user"; #select query
        $dbConn->execute($selectFull, $result);
        while ($row = mysqli_fetch_array($result)) {
            $val[] = $row;
        }
        return $val;
    }

    function getUserById($id) { #get user by ir
        $val = [];
        $dbConn = new mySQL();
        $selectFull = "select * from tbl_user where id=$id"; #select query
        $dbConn->execute($selectFull, $result);
        while ($row = mysqli_fetch_array($result)) {
            $val[] = $row;
        }
        return $val;
    }

    function addUser($dataValue) {
        $name = $dataValue['UserName'];
        $Upassword = $dataValue['Upassword'];
        $UFullName = $dataValue['UFullName'];

        $dbConn = new mySQL();
        $selectFull = "INSERT INTO tbl_user (user_name,password,full_name,status) 
		VALUES('$name','" . base64_encode($Upassword) . "','$UFullName',1)
		";               #insert query
        $dbConn->execute($selectFull, $result);
        return $dbConn;
    }

    function updateUser($dataValue) {
        $id = $dataValue['Uid'];
        $name = $dataValue['UserName'];
        $Upassword = $dataValue['Upassword'];
        $UFullName = $dataValue['UFullName'];
        $staus = $dataValue['Ustatus'];

        $dbConn = new mySQL();

        if (!empty($Upassword)) {
            $selectFull = "UPDATE tbl_user
                        SET user_name = '$name',password = '" . base64_encode($Upassword) . "',
                            full_name = '$UFullName' ,status = $staus
                        WHERE id= $id";
        } else {
            $selectFull = "UPDATE tbl_user
                        SET user_name = '$name',full_name = '$UFullName' ,status = $staus
                        WHERE id= $id";
        }
        //echo $selectFull; exit;
        $dbConn->execute($selectFull, $result);
        return $dbConn;
    }

    function updatePAssword($id, $password) {
        $dbConn = new mySQL();
        $selectFull = "UPDATE tbl_user
                        SET password = '" . $password . "'
                        WHERE id= $id";
        $dbConn->execute($selectFull, $result);
        return $dbConn;
    }
    
    function updatePAsswordByUserName($userName, $password) {
        $dbConn = new mySQL();
        $selectFull = "UPDATE tbl_user
                        SET password = '" . $password . "'
                        WHERE user_name = '$userName'";
        $dbConn->execute($selectFull, $result);
        return $dbConn;
    }

}

?>