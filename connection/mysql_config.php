<?php

/*
 * mysql_config
 * 2016-08-30
 * kaushalya
 */

class mySQL {

    var $last_error;
    var $last_errornumber;
    var $dbServer;
    var $dbLogonName;
    var $dbPassword;
    var $dbDatabase;
    var $con;
    var $db;

    function __construct()
    {

        $this->dbServer = "";
        $this->dbLogonName = "";
        $this->dbPassword = "";
        $this->dbDatabase = "";

        $last_error = "";
        $last_errornumber = 0;
    }

    function getLastError()
    {
        return $this->last_error;
    }

    function getLastErrornumber()
    {
        return $this->last_errornumber;
    }

    function halt()
    {
        echo('<hr>');
        echo('ERROR (' . $this->last_errornumber . ')');
        echo(' ' . $this->last_error);
        echo('<hr>');
        exit;
    }

    function execute($sql, &$result)
    {
        $result = false;

        $this->con = mysqli_connect($this->dbServer, $this->dbLogonName, $this->dbPassword);

        if (!$this->con) {            
            $this->last_error = mysqli_connect_error();
            $this->last_errornumber = mysql_errno();
            $this->halt();
        }

        $this->db = mysqli_select_db($this->con, $this->dbDatabase) ;//or die("error select db");
        
        if (!$this->db) {
            $this->last_error = mysqli_connect_error();
            $this->last_errornumber = mysql_errno();
            $this->halt();
        }

        
        $result = mysqli_query($this->con, $sql);

        if (!$result) {
            //return false;
            $this->last_error = mysql_error();
            $this->last_errornumber = mysql_errno();
            $this->halt();
        }

        mysqli_close($this->con);

        $this->last_error = 'Execution was sucessfull';
        $this->last_errornumber = 0;
        return true;
    }

    // function execute_with_id($sql, &$result, &$insert_id)
    // {
    //     $result = false;

    //     $con = mysql_pconnect($this->dbServer, $this->dbLogonName, $this->dbPassword);
    //     if (!$con) {
    //         $this->last_error = mysql_error();
    //         $this->last_errornumber = mysql_errno();
    //         $this->halt();
    //     }

    //     $db = mysql_select_db($this->dbDatabase, $con);
    //     if (!$db) {
    //         $this->last_error = mysql_error();
    //         $this->last_errornumber = mysql_errno();
    //         $this->halt();
    //     }

    //     $result = mysql_query($sql, $con);
    //     if (!$result) {
    //         $this->last_error = mysql_error();
    //         $this->last_errornumber = mysql_errno();
    //         $this->halt();
    //     }

    //     $insert_id = mysql_insert_id($con);

    //     mysql_close($con);

    //     $this->last_error = 'Execution was sucessfull';
    //     $this->last_errornumber = 0;
    //     return true;
    // }

}

//:mySQL
?>
