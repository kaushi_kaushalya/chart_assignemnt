<?php

error_reporting(0);
session_start();

require_once("../connection/mysql_config.php");
include("../module/mod_user.php");

$modUser = new mod_user();

$postValue = $_POST;
$redirectPage = $postValue['redirectPage'];

$teacherexit = TRUE;
if (isset($_POST['UTeacher']) && !empty($_POST['UTeacher'])) {
    $checkTheTeacherExit = $modUser->checkTheTeacherExit($_POST['UTeacher']);
    if (!empty($checkTheTeacherExit)) {
        $teacherexit = FALSE;
    }
}

if (!isset($postValue['Uid']) || empty($postValue['Uid'])) {
    if (!empty($postValue['UFullName']) && !empty($_POST['UserName']) && !empty($_POST['Upassword'])) {
        $userDetails = $modUser->getlogedUserDetails($_POST['UserName']);
        if (!empty($userDetails)) {
            $code = 0;
            $message = 'User Name Already Exit !';
        } else {
            if ($teacherexit) {
                $addUpdateUserDetails = $modUser->addUser($postValue);
                if ($addUpdateUserDetails) {
                    $code = 1;
                    $message = 'Successfully Done !';
                } else {
                    $code = 0;
                    $message = 'Execution Fail !';
                }
            } else {
                $code = 0;
                $message = 'Cannot Add Same Teacher twice !';
            }
        }
    } else {
        $code = 0;
        $message = 'All Fields Required !';
    }
} else {
    if (!empty($postValue['UFullName']) && !empty($_POST['UserName'])) {
        if ($teacherexit) {
            $addUpdateUserDetails = $modUser->updateUser($postValue);
            if ($addUpdateUserDetails) {
                $code = 1;
                $message = 'Successfully Done !';
            } else {
                $code = 0;
                $message = 'Execution Fail !';
            }
        } else {
            $code = 0;
            $message = 'Cannot Add Same Teacher twice !';
        }
    } else {
        $code = 0;
        $message = 'User Name and Full Name are required !';
    }
}
$_SESSION['addViewUser'] = ['code' => $code, 'message' => $message];
header('location: '.$redirectPage);
?>

