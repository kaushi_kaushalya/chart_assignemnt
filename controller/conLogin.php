<?php

/*
 *  conLogin.php | class
 * 2019-10-07
 * kashalya
 */

//error_reporting(0);
session_start();
include("../connection/mysql_config.php");
include("../module/mod_user.php");
include("../config/paramsLocal.php");

$modUser = new mod_user();

$userName = trim($_POST['userName']);
$pword = trim($_POST['password']);

$val = [ 'success' => false,'info' =>'error in login','data'=> []];

if (!empty($userName) && !empty($pword)) {
    // retrive data from db
    $userDetails = $modUser->getlogedUserDetails($userName, $result);
    
    if ($userDetails && $userDetails['success']) {
        $LogedUser = $userDetails['data'];
        
        // checking passoed
        if (base64_decode($LogedUser['password']) == $pword) {
            $_SESSION['emsLogedUser'] = $LogedUser;
            
            // checking role
            $_SESSION['emsLogedUser']['isAdmin'] = true;
            $val = [ 'success' => true,'info' =>'Sucess','data'=> []];
        } else {
            $val = [ 'success' => false,'info' =>'Invalid user detsils','data'=> []];
        }
    } else {
        $val = [ 'success' => false,'info' => 'Invalid user detsils','data'=> []];
    }
} else {
    $val = [ 'success' => false,'info' =>'User NAme and Password Should Not Empty','data'=> []];
}

echo json_encode($val); exit;
?>