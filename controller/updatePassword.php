<?php
error_reporting(0);
session_start();

require_once("../connection/mysql_config.php");
include("../module/mod_user.php");

$modUser = new mod_user();

$postValue = $_POST;

if (!empty($postValue['opassword']) && !empty($postValue['npassword']) && !empty($postValue['cpassword'])) {
    if ($postValue['npassword'] == $postValue['cpassword']) {
        $getUser = $modUser->getUserById($postValue['Uid']);
        $oldPassword = base64_decode($getUser[0]['password']);
        if ($oldPassword == $postValue['opassword']) {
            $updatePassword = $modUser->updatePAssword($postValue['Uid'], base64_encode($postValue['npassword']));
            if ($updatePassword) {
                $code = 1;
                $message = 'Successfully Updated ! ';
            } else {
                $code = 0;
                $message = 'Execution Fail ! ';
            }
        } else {
            $code = 0;
            $message = 'Old Password is not Correct ! ';
        }
    } else {
        $code = 0;
        $message = 'Confirm password and New password must be same ';
    }
} else {
    $code = 0;
    $message = 'All Fields Required !';
}

$_SESSION['changePassword'] = ['code' => $code, 'message' => $message];
header("location: ../view/viewChangePassword.php");
?>