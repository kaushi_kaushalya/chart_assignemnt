<?php

error_reporting(0);
session_start();

require_once("../connection/mysql_config.php");
include("../module/mod_user.php");
include("../config/paramsLocal.php");
include('../PHPMailer/PHPMailerAutoload.php');

$mail = new PHPMailer;

$modUser = new mod_user();

$emailParams = $paramsLocal['emailParams'];
$password = $paramsLocal['commonPassword'];

$postValue = $_POST;

$userName = $postValue['userName'];

if (!empty($userName)) {
    $getlogedUserDetails = $modUser->getlogedUserDetails($userName);
    if (!empty($getlogedUserDetails)) {
        $encodePassword = base64_encode($password);
        $updatePAsswordByUserName = $modUser->updatePAsswordByUserName($userName, $encodePassword);
        if ($updatePAsswordByUserName) {
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $emailParams['host'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = $emailParams['SMTPAuth'];                               // Enable SMTP authentication
            $mail->Username = $emailParams['userName'];                 // SMTP username
            $mail->Password = $emailParams['password'];                           // SMTP password
            $mail->SMTPSecure = $emailParams['SMTPSecure'];                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $emailParams['port'];
            $mail->setFrom($emailParams['fromEmail'], 'Mailer');
            $mail->addAddress($userName, 'Hi');
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Password Details';

            $mail->Body = " Your Password Successfully Change , This is your currnet Password <b>Asdf2017zxcV</b> ";
           
            $mail->AltBody = 'This is auto geneated email!';
            
            if ($mail->send()) {
                $code = 1;
                $message = 'Your password sent to your email!';
            } else {
                $code = 0;
                $message = 'Please Try Again !';
            }
        } else {
            $code = 0;
            $message = 'Execution Fail !';
        }
    } else {
        $code = 0;
        $message = 'Invalid  Email Address !';
    }
} else {
    $code = 0;
    $message = 'Email Address Can not Be empty !';
}

$_SESSION['changePassword'] = ['code' => $code, 'message' => $message];
header("location: ../index.php");
?>
