/**
 * Author:  kaushalya
 * Created: 2019-10-07
 * database chagers to apply in live deployement
 */

--
-- Table structure for table `exam_user`
--

CREATE TABLE `exam_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `full_name` varchar(500) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_user`
--

INSERT INTO `exam_user` (`id`, `user_name`, `password`, `full_name`, `status`, `teacherId`) VALUES
(1, 'kaushi', 'MTIzNA==', 'kaushi Kaushalya H', 1, NULL),
(2, 'nimmi', 'MTIzNA==', 'nimmY', 1, 2);