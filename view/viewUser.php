<?php
include('../template/header.php');
include('../template/menu.php');
include('../template/headerTab.php');

require_once("../connection/mysql_config.php");
include("../module/mod_user.php");
include("../config/paramsLocal.php");

$modUser = new mod_user();
$getTecherList = $modUser->getUserList();

$adminUser = $paramsLocal['adminUser'];
?>

<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="col-md-12 col-sm-12 col-xs-12">
    <?php
    if (isset($_SESSION['addViewUser'])) {
        $addViewUser = $_SESSION['addViewUser'];
        ?>
        <div class="alert <?= ($addViewUser['code'] == 1) ? 'alert-success' : 'alert-danger' ?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= ($addViewUser['code'] == 1) ? 'Success' : 'Error' ?>!</strong> <?= $addViewUser['message']; ?>
        </div>
        <?php
        unset($_SESSION['addViewUser']);
    }
    ?>
    <div class="x_panel">
        <div class="x_title">
            <h2>User<small>List</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li> <button class="btn btn-sm btn-info" onclick='addAndViewUser("ADD", -1)'>Add User</button></li>
                <li class="dropdown"> &nbsp;</li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($getTecherList as $key => $Techer) {
                                    $id = $Techer['id'];
                                    ?>
                                    <tr>
                                        <td><?= $Techer['user_name'] ?></td>
                                        <td><?= $Techer['full_name'] ?></td>
                                        <td><?= ($Techer['status']) ? 'Active' : 'Inactive'; ?></td>
                                        <td><a href="#"><i class="fa fa-eye" onclick='addAndViewUser("VIEW", "<?= $id; ?>")'></a></i> &nbsp; <a href="#"> <?php if(!in_array($Techer['user_name'], $adminUser)){ ?><i class="fa fa-pencil" onclick='addAndViewUser("UPDATE", "<?= $id; ?>")'></a></i><?php } ?></td>
                                    </tr> 
                                    <?php
                                }
                                ?>                                                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="teacher-details-modal">
    <div class="modal-dialog">
        <div class="modal-content" id="teacher-modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
include('../template/foter.php');
?>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

<script>
    function addAndViewUser(action, id) {
        var dataValue = {
            'action': action,
            'id': id,
        };
        $.ajax({
            type: "POST",
            url: 'subPage/addUser.php',
            cache: false,
            data: dataValue,
            dataType: "html",
            success: function (jsonresponce) {
                if (jsonresponce) {
                    $("#teacher-modal-content").html(jsonresponce);
                    jQuery('#teacher-details-modal').modal('show');
                } else {
                    return false;
                }
            }
        });
    }
</script>


