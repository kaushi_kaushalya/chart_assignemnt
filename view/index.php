<?php
include('../template/header.php');
include('../template/menu.php');
include('../template/headerTab.php');
?>


<h1 align='center'>  UCM <?= date('Y'); ?> </h1><br/><br/>
<div class="row top_tiles">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12"><div class="tile"><br/></div></div>
    <div id="container" style="width:100%; height:400px;"></div>
    <div id="containerapplication" style="width:100%; height:400px;"></div>
    <div id="containeraccepted" style="width:100%; height:400px;"></div>
</div>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>


<?php
include('../template/foter.php');
?>

<script>

$( document ).ready(function() {
    getChart();
});

function getChart(){
    var dataValue = {
    };
    $.ajax({
        type: "POST",
        url: '../controller/chart.php',
        cache: false,
        data: dataValue,
        dataType: "json",
        success: function (jsonresponce) {
            if(jsonresponce.success){
                displaypresentageChart(jsonresponce.data.presentage);
                displayapplicationChart(jsonresponce.data.application);
                displayacceptedChart(jsonresponce.data.accepted);
            }else{
                alert('Error in getting chart details');
            }
        }
    });
}

function displaypresentageChart(serial){

    Highcharts.chart('container', {

    title: {
        text: 'Upcase retention curve'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: ''
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 0
        }
    },

    series:  serial ,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

    });
}

function displayapplicationChart(serial){

    Highcharts.chart('containerapplication', {

        title: {
            text: 'Upcase retention curve - application'
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: true
                },
                pointStart: 0
            }
        },

        series:  serial ,

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });
}

function displayacceptedChart(serial){

Highcharts.chart('containeraccepted', {

    title: {
        text: 'Upcase retention curve - accepted'
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: ''
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: true
            },
            pointStart: 0
        }
    },

    series:  serial ,

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});
}
</script>
