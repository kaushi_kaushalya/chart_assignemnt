<?php
include('../template/header.php');
include('../template/menu.php');
include('../template/headerTab.php');
?>

<h2> Hi <?= $_SESSION['emsLogedUser']['fullName']; ?> </h2><br/><br/>

<?php
if (isset($_SESSION['changePassword'])) {
    $changePassword = $_SESSION['changePassword'];
    ?>
    <div class="alert <?= ($changePassword['code'] == 1) ? 'alert-success' : 'alert-danger' ?>">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><?= ($changePassword['code'] == 1) ? 'Success' : 'Error' ?>!</strong> <?= $changePassword['message']; ?>
    </div>
    <?php
    unset($_SESSION['changePassword']);
}
?>

<div class="col-lg-10">
    <div class="x_panel">    
        <div class="x_title">
            <h2>Change Password <small><?= $action; ?> FORM</small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form class="form-horizontal form-label-left" action="../controller/updatePassword.php" method="POST">
                <input type="hidden" class="form-control" id="Uid" name="Uid" value="<?= $_SESSION['emsLogedUser']['id']; ?>">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Old Password <span class="text-danger"> * </span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="password" class="form-control" placeholder="Old Password" id="opassword" name="opassword" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">New Password <span class="text-danger"> * </span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="password" class="form-control" placeholder="New Password" id="npassword" name="npassword" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="text-danger"> * </span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="password" class="form-control" placeholder="Confirm Password" id="cpassword" name="cpassword" />
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success pull-right"> Change Password</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('../template/foter.php');
?>
