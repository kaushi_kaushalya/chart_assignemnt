<?php
error_reporting(0);
require_once("../../connection/mysql_config.php");
include("../../module/mod_user.php");
include("../../module/mod_teacher.php");

$modUser = new mod_user();

$id = $_POST['id'];
$action = $_POST['action'];

if ($action == 'ADD') {
    $readOnly = FALSE;
    $id = '';
    $userName = '';
    $password = '';
    $fullName = '';
    $Uid = '';
    $teacherId = '-1';
} else {
    $getUserList = $modUser->getUserById($id);
    $Uid = $getUserList[0]['id'];
    $userName = $getUserList[0]['user_name'];
    $password = '';
    $fullName = $getUserList[0]['full_name'];
    $status = $getUserList[0]['status'];
    $teacherId = $getUserList[0]['teacherId'];

    if ($action == 'VIEW') {
        $readOnly = TRUE;
    } else {
        $readOnly = FALSE;
    }
}
?>
<div class="x_panel">
    <div class="x_title">
        <h2>User Detail Form <small><?= $action; ?> DETAILS</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form class="form-horizontal form-label-left" action="../controller/addUpdateUserDetails.php" method="POST">
            <input type="hidden" class="form-control" id="Uid" name="Uid" value="<?= $Uid; ?>">
            <input type="hidden" id="redirectPage" name="redirectPage" value="../view/viewUser.php" />
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name <?php if ($action != 'VIEW') { ?> <span class="text-danger">* </span><?php } ?></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" <?= ($readOnly) ? 'readonly="readOnly"' : '' ?> placeholder="Full Name" id="UFullName" name="UFullName" value="<?= $fullName; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">User Name <?php if ($action != 'VIEW') { ?> <span class="text-danger">* </span><?php } ?></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" <?php if ($action != 'ADD') { ?>readonly="readOnly" <?php } ?> placeholder="User Name" id="UserName" name="UserName" value="<?= $userName; ?>">
                </div>
            </div>
            <?php
            if ($action != 'VIEW') {
                ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Password<?php if ($action == 'ADD') { ?> <span class="text-danger">*</span> <?php } ?></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="password" class="form-control" <?= ($readOnly) ? 'readonly="readOnly"' : '' ?> placeholder="Password" id="Upassword" name="Upassword" value="<?= $password; ?>">
                    </div>
                </div>
                <?php
            }
            ?>
            <?php
            if ($action != 'ADD') {
                ?>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" <?= ($readOnly) ? 'readonly="readOnly"' : '' ?> name="Ustatus" >
                            <option value="1" <?= ($status == 1) ? 'selected' : ''; ?> >Active</option>
                            <option value="0" <?= ($status != 1) ? 'selected' : ''; ?> >Inactive</option>
                        </select>

                    </div>
                </div>
                <?php
            }
            ?>
            <div class="form-group pull-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php
                if ($action != 'VIEW') {
                    ?>
                    <button type="submit" class="btn btn-primary">Continue</button>
                    <?php
                }
                ?>
            </div>
        </form>
    </div>
</div>
