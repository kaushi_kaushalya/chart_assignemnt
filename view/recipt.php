<?php
if (!isset($_GET['type'])) {
    header('location:index.php');
}
include('../template/header.php');
include('../template/menu.php');
include('../template/headerTab.php');


require_once("../connection/mysql_config.php");
include("../module/mod_receipt.php");

$modReceipt = new mod_receipt();

$type = $_GET['type'];
$getReciptDetails = $modReceipt->getReceiptDetails(['type' => $type]);

?>


<!-- Datatables -->
<link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="col-md-12 col-sm-12 col-xs-12">
    <?php
    if (isset($_SESSION['addViewRecipt'])) {
        $addViewRecipt = $_SESSION['addViewRecipt'];
        ?>
        <div class="alert <?= ($addViewRecipt['code'] == 1) ? 'alert-success' : 'alert-danger' ?>">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= ($addViewRecipt['code'] == 1) ? 'Success' : 'Error' ?>!</strong> <?= $addViewRecipt['message']; ?>
        </div>
        <?php
        unset($_SESSION['addViewRecipt']);
    }
    ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?php if ($type == 1) {
        echo 'Music';
    } else if ($type == 2) {
        echo 'Speach';
    } ?> Receipt <small>List</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li> <button class="btn btn-sm btn-info" onclick='ViewReceipt("ADD", -1)'>Add</button></li>
                <li> <button class="btn btn-sm btn-primary" id='refresh' >Refresh</button></li>
                <li class="dropdown"> &nbsp;</li>
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            </ul>            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Grate</th>
                                    <th>Subject</th>
                                    <th>Center</th>
                                    <th>Fees</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($getReciptDetails as $key => $recipt) {
                                    $id = $recipt['id'];
                                    $subject = $recipt['subject'];
                                    $center = $recipt['center'];
                                    $grate = $recipt['grate'];
                                    $user = $recipt['user'];
                                    $student = $recipt['student'];
                                    $fees = $recipt['fees'];
                                    $date = $recipt['date'];
                                    $status = $recipt['status'];
                                    ?>
                                    <tr>
                                        <td><?= $student ?></td>
                                        <td><?= $grate ?></td>
                                        <td><?= $subject ?></td>
                                        <td><?= $center ?></td>
                                        <td><?= $fees ?></td>
                                        <td><?= $user ?></td>
                                        <td><?= $date ?></td>
                                        <td><?= ($status) ? 'Active' : 'Inactive'; ?></td>
                                        <td><a href="#"><i class="fa fa-eye" onclick='ViewReceipt("VIEW", "<?= $id; ?>")'></a></i> &nbsp; <i><a href="#"> <i class="fa fa-pencil" onclick='ViewReceipt("UPDATE", "<?= $id; ?>")'></a></i></td>
                                    </tr> 
                                    <?php
                                }
                                ?>                                                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="receapt-details-modal">
    <div class="modal-dialog">
        <div class="modal-content" id="receapt-modal-content">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php
include('../template/foter.php');
?>
<!-- Datatables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="../vendors/jszip/dist/jszip.min.js"></script>
<script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="../vendors/pdfmake/build/vfs_fonts.js"></script>


<script>
                                            $(document).ready(function () {
                                                $('#refresh').click(function () {
                                                    location.reload();
                                                });
                                            });
                                            function ViewReceipt(action, id) {
                                                var dataValue = {
                                                    'action': action,
                                                    'id': id,
                                                    'type': <?= $type; ?>,
                                                };
                                                $.ajax({
                                                    type: "POST",
                                                    url: 'subPage/updateReceapt.php',
                                                    cache: false,
                                                    data: dataValue,
                                                    dataType: "html",
                                                    success: function (jsonresponce) {
                                                        if (jsonresponce) {
                                                            $("#receapt-modal-content").html(jsonresponce);
                                                            jQuery('#receapt-details-modal').modal('show');
                                                        } else {
                                                            return false;
                                                        }
                                                    }
                                                });
                                            }
</script>
