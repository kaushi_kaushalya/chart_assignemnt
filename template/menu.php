<?php
/*
 * nav bar
 * kaushalya5054@gmail.com
 * 2017-03-12
 */
?>
<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li><a href="index.php"><i class="fa fa-home"></i> Home </a></li>
                <li><a href="viewUser.php"><i class="fa fa-user"></i> User Management </a></li>
        </ul>
    </div>
</div>
<!-- /sidebar menu -->