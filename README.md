# README #

This README would normally document whatever steps are necessary to get your application up and running.

## project configuration ##

1. put the project folder in web directory

2. create database and run the SQL (project_folder/databaseScript/sql-1.sql)

3.config the data base details in configuration file (project_folder/connection/mysql_config.php)
       1. dbServer -> database server
	   2. dbLogonName -> database login user
	   3. dbPassword -> database login user password
	   4. dbDatabase -> database name
	   
4. config the params details if need only (project_folder/config/paramsLocal.php)

5. config the csv file path in project_folder/controller/chart.php

6. This project will run in the Linux environment , (code didn't written for support all the os)
