<?php
error_reporting(0);
session_start();

require_once("connection/mysql_config.php");

if (isset($_SESSION['emsLogedUser'])) {
    unset($_SESSION['emsLogedUser']);
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UCM | </title>

        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- Animate.css -->
        <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="build/css/custom.min.css" rel="stylesheet">

        <style>
            #div1{
                width: 400px;
                position: relative;
                z-index: 1;
                background: #FFFFFF;
                max-width: 360px;
                margin: 0 auto 100px;
                padding: 45px;
                text-align: center;
                box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
            }
            #buttonId{
                font-family: "Roboto", sans-serif;
                text-transform: uppercase;
                outline: 0;
                background: #7e7e7e;
                width: 100%;
                border: 0;
                padding: 15px;
                color: #FFFFFF;
                font-size: 14px;
                -webkit-transition: all 0.3 ease;
                transition: all 0.3 ease;
                cursor: pointer;
            }
        </style>
    </head>

    <body class="login" style="background-color:#eae8e7;">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                <div class="animate form login_form" id="login" style="width:400px;padding: 6% 0 0; margin: auto;">
                    <section class="login_content" id="div1">
                        <?php
                        if (isset($_SESSION['changePassword'])) {
                            $changePassword = $_SESSION['changePassword'];
                            ?>
                            <div class="alert <?= ($changePassword['code'] == 1) ? 'alert-success' : 'alert-danger' ?>">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?= $changePassword['message']; ?>
                            </div>
                            <?php
                            unset($_SESSION['changePassword']);
                        }
                        ?>
                        <form action="#" method="post" id="loginForm">
                            <h1>Login Form</h1>
                            <div>
                                <input type="text" class="form-control" id="userName" name="userName" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="" />
                            </div>
                            <div>
                                <button type="button" id="logInbuttonId" class="btn btn-default submit" > L o g i n </button>
                            </div>
                                <p class="text-danger" id="loginMessageP" hidden='true'>Error in login</p>
                            <div class="separator">

                                <p class="change_link">
                                    <a href="#" id="changePasswordHref" class="to_register">Change Password</a>
                                    &nbsp; &nbsp; &nbsp; 
                                    <a href="#signup" class="to_register">I want to Register</a>
                                </p>
                            </div>
                        </form>
                    </section>
                </div>                                 


                <div id="register" class="animate form registration_form">
                    <?php
                    if (isset($_SESSION['addViewUser'])) {
                        $addViewUser = $_SESSION['addViewUser'];
                        ?>
                        <div class="alert <?= ($addViewUser['code'] == 1) ? 'alert-success' : 'alert-danger' ?>">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong><?= ($addViewUser['code'] == 1) ? 'Success' : 'Error' ?>!</strong> <?= $addViewUser['message']; ?>
                        </div>
                        <?php
                        unset($_SESSION['addViewUser']);
                    }
                    ?>
                    <section class="login_content" id="div1">
                        <form action="controller/addUserAndTeacherDetails.php" method="POST">
                            <h1>Create Account</h1>
                            <input type="hidden" id="redirectPage" name="redirectPage" value="../index.php#signup" />
                            <input type="hidden" id="UTeacher" name="UTeacher" value="null" />
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" placeholder="Full Name" id="TName" name="TName" >
                                </div>
                            </div>
                            <div class="form-group">
<!--                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Password <span class="text-danger">*</span></label>-->
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="password" class="form-control"  placeholder="Password" id="Upassword" name="Upassword">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-1 col-sm-1 col-xs-1"> <input type="checkbox" > </label>
                                <div class="col-md-11 col-sm-11 col-xs-12">                                   
                                    I accept the User Agreement     <br/> &nbsp;                                
                                </div>
                            </div>

                            <div class="form-group"><br/>
                                <button type="button" class="btn btn-default submit">Reset</button>
                                <button type="submit" style='width:190px; font-family: "Roboto", sans-serif;  background: #7e7e7e; color: #FFFFFF;' class="btn btn-default submit">Register</button>
                            </div>
                            <div class="separator">
                                <p class="change_link">Already a member ?
                                    <a href="#signin" class="to_register"> Log in </a>
                                </p>
                            </div>
                        </form>
                    </section>
                </div>

            </div>

        </div>
        <div class="col-lg-12">
            <div class="col-lg-4 col-sm-6">&nbsp;</div>
            <div id="changePassworddiv" class="col-lg-5 col-sm-6" style="width: 400px;padding: 8% 0 0; margin: auto;">
                <section class="login_content" id="div1">
                    <form action="controller/ChangePasswordThroughtEmail.php" method="post" id="changePasswordForm">
                        <h1>New Password</h1>
                        <div>
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="Email " required="" />
                        </div>
                        <div>
                            <button type="submit" id="buttonId" class="btn btn-default submit" >Change Password </button>
                        </div>

                        <div class="separator">

                            <p class="change_link"> All Ready Member ?                                 
                                <a href="#"  id="to_register"> Login </a>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        <!-- jQuery -->
        <script src="vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#changePassworddiv").hide();
                $('#changePasswordHref').click(function (e) {
                    $('#login').hide();
                    $('#register').hide();
                    $('#changePassworddiv').show();
                });

                $('#to_register').click(function (e) {
                    location.reload();
                });
            });

            $('#logInbuttonId').click( function(){
                var dataValue = {
                    'userName': $('#userName').val(),
                    'password': $('#password').val(),
                };
                $.ajax({
                    type: "POST",
                    url: 'controller/conLogin.php',
                    cache: false,
                    data: dataValue,
                    dataType: "json",
                    success: function (jsonresponce) {
                        //console.log(jsonresponce.success);
                        if (!jsonresponce.success) {
                            $("#loginMessageP").html(jsonresponce.info);
                            $('#loginMessageP').show();
                        } else {
                            window.location.href = "view/index.php";
                        }
                    }
                });
            })
        </script>
    </body>
</html>
